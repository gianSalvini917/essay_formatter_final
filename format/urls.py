from django.urls import path
from .views import HomePageView, SelectStylePageView, MLAifCitationPageView, MLAInputPageView, MLADetailPageView, MLACitationInfoView,MLACitationInputPageView,  MLAWithCitationPageView, APADetailPageView, APAInputPageView, APAifCitationPageView, APAWithCitationPageView, APACitationInputPageView, APACitationInfoView, CHICitationInfoView, CHICitationInputPageView, CHIWithCitationPageView, CHIDetailPageView, CHIInputPageView, CHIifCitationPageView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
	######### APA #########
	## APA with Citation urls ##
	path('select-style/chi/citation-ex/', CHICitationInfoView.as_view(), name='chi-citation-ex'),
	path('select-style/chi-citation/new/', CHICitationInputPageView.as_view(), name='chi-citation-new'),
	path('select-style/chi-citation/<int:pk>/', CHIWithCitationPageView.as_view(), name='chi-citation-details'),
	## APA no citation urls ##
	path('select-style/chi/<int:pk>/', CHIDetailPageView.as_view(), name='chi-details'),
	path('select-style/chi/new/', CHIInputPageView.as_view(), name='chi-new'),
	## ------------------- ##
	path('select-style/chi/', CHIifCitationPageView.as_view(), name='select-chi'),
	#######################

	######### APA #########
	## APA with Citation urls ##
	path('select-style/apa/citation-ex/', APACitationInfoView.as_view(), name='apa-citation-ex'),
	path('select-style/apa-citation/new/', APACitationInputPageView.as_view(), name='apa-citation-new'),
	path('select-style/apa-citation/<int:pk>/', APAWithCitationPageView.as_view(), name='apa-citation-details'),
	## APA no citation urls ##
	path('select-style/apa/<int:pk>/', APADetailPageView.as_view(), name='apa-details'),
	path('select-style/apa/new/', APAInputPageView.as_view(), name='apa-new'),
	## ------------------- ##
	path('select-style/apa/', APAifCitationPageView.as_view(), name='select-apa'),
	#######################

	######### MLA #########
	## MLA with Citation urls ##
	path('select-style/mla/citation-ex/', MLACitationInfoView.as_view(), name='mla-citation-ex'),
	path('select-style/mla-citation/new/', MLACitationInputPageView.as_view(), name='mla-citation-new'),
	path('select-style/mla-citation/<int:pk>/', MLAWithCitationPageView.as_view(), name='mla-citation-details'),
	## MLA no citation urls ##
	path('select-style/mla/<int:pk>/', MLADetailPageView.as_view(), name='mla-details'),
	path('select-style/mla/new/', MLAInputPageView.as_view(), name='mla-new'),
	## -------------------- ##
	path('select-style/mla/', MLAifCitationPageView.as_view(), name='select-mla'),
	#######################

	path('select-style/', SelectStylePageView.as_view(), name='select-style'),
	path('', HomePageView.as_view(), name='home'),
]

urlpatterns+= staticfiles_urlpatterns()