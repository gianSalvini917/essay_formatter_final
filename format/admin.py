from django.contrib import admin
from .models import MLAEssay, MLAEssayWCitation, APAEssay, APAEssayWCitation, CHIEssay, CHIEssayWCitation
# Register your models here.

admin.site.register(MLAEssay)
admin.site.register(MLAEssayWCitation)
admin.site.register(APAEssay)
admin.site.register(APAEssayWCitation)
admin.site.register(CHIEssay)
admin.site.register(CHIEssayWCitation)