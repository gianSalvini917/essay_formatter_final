from django.views.generic import TemplateView, DetailView
from django.views.generic.edit import CreateView

from .models import MLAEssay, MLAEssayWCitation, APAEssay, APAEssayWCitation, CHIEssay, CHIEssayWCitation


class HomePageView(TemplateView):
	template_name= 'home.html'
	
class SelectStylePageView(TemplateView):
	template_name = 'selectstyle.html'

######## MLA #########

class MLAifCitationPageView(TemplateView):
	template_name ='MLAifCitation.html'

## MLA with citations ##
class MLACitationInfoView(TemplateView):
	template_name = 'MLACitationEx.html'

class MLACitationInputPageView(CreateView):
	model = MLAEssayWCitation
	fields = '__all__'
	template_name = 'mla-citation.html'

class MLAWithCitationPageView(DetailView):
	model = MLAEssayWCitation
	template_name = 'mla-citation-details.html'
## ------------------ ##


## MLA no citations ##
class MLAInputPageView(CreateView):
	model = MLAEssay
	fields = '__all__'
	template_name = 'mla.html'


class MLADetailPageView(DetailView):
	model = MLAEssay
	template_name = 'mla-details.html'

## ----------------- ##

###################

###### APA  #########

class APAifCitationPageView(TemplateView):
	template_name = 'APAifCitation.html'

##### APA with Citation ######
class APACitationInfoView(TemplateView):
	template_name = 'APACitationEx.html'

class APACitationInputPageView(CreateView):
	model = APAEssayWCitation
	fields = '__all__'
	template_name = 'apa-citation.html'

class APAWithCitationPageView(DetailView):
	model = APAEssayWCitation
	template_name = 'apa-citation-details.html'	
## ----------------- ##
## APA no citations ##
class APAInputPageView(CreateView):
	model = APAEssay
	fields = '__all__'
	template_name = 'apa.html'


class APADetailPageView(DetailView):
	model = APAEssay
	template_name = 'apa-details.html' 
## ---------------- ##

####### Chicago ###########
class CHIifCitationPageView(TemplateView):
	template_name ='CHIifCitation.html'
##### Chicago with Citation ######
class CHICitationInfoView(TemplateView):
	template_name = 'CHICitationEx.html'

class CHICitationInputPageView(CreateView):
	model = CHIEssayWCitation
	fields = '__all__'
	template_name = 'chi-citation.html'

class CHIWithCitationPageView(DetailView):
	model = CHIEssayWCitation
	template_name = 'chi-citation-details.html'	
## ----------------- ##
## Chicago no citations ##
class CHIInputPageView(CreateView):
	model = CHIEssay
	fields = '__all__'
	template_name = 'chi.html'


class CHIDetailPageView(DetailView):
	model = CHIEssay
	template_name = 'chi-details.html' 
## ---------------- ##
