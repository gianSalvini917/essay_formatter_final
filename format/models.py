from django.db import models
from django.urls import reverse

# Create your models here.
class MLAEssay(models.Model):
	author = models.CharField(max_length = 100)
	professor = models.CharField(max_length = 100)
	className = models.CharField(max_length = 100, verbose_name="Class Name")
	date = models.CharField(max_length = 100)
	title = models.CharField(max_length = 100)
	paragraphs = models.TextField()

	def __str__(self):
		return self.title[:50]

	def get_absolute_url(self):
		return reverse('mla-details', args=[str(self.id)])

class MLAEssayWCitation(models.Model):
	author = models.CharField(max_length = 100)
	professor = models.CharField(max_length = 100)
	className = models.CharField(max_length = 100, verbose_name="Class Name")
	date = models.CharField(max_length = 100)
	title = models.CharField(max_length = 100)
	paragraphs = models.TextField()
	citations = models.TextField()

	def __str__(self):
		return self.title[:50]

	def get_absolute_url(self):
		return reverse('mla-citation-details', args=[str(self.id)])

class APAEssay(models.Model):
	title = models.CharField(max_length = 100)
	author = models.CharField(max_length = 100)
	className = models.CharField(max_length = 100, verbose_name="Your Institution")
	abstract = models.TextField()
	keywords = models.CharField(max_length = 100)
	paragraphs = models.TextField()

	def __str__(self):
		return self.title[:50]

	def get_absolute_url(self):
		return reverse('apa-details', args=[str(self.id)])

class APAEssayWCitation(models.Model):
	title = models.CharField(max_length = 100)
	author = models.CharField(max_length = 100)
	className = models.CharField(max_length = 100, verbose_name="Your Institution")
	abstract = models.TextField()
	keywords = models.CharField(max_length = 100)
	paragraphs = models.TextField()
	citations = models.TextField()

	def __str__(self):
		return self.title[:50]

	def get_absolute_url(self):
		return reverse('apa-citation-details', args=[str(self.id)])

class CHIEssay(models.Model):
	title = models.CharField(max_length = 100)
	author = models.CharField(max_length = 100)
	professor = models.CharField(max_length = 100)
	className = models.CharField(max_length = 100, verbose_name="Class Name")
	date = models.CharField(max_length = 100)
	paragraphs = models.TextField()

	def __str__(self):
		return self.title[:50]

	def get_absolute_url(self):
		return reverse('chi-details', args=[str(self.id)])

class CHIEssayWCitation(models.Model):
	author = models.CharField(max_length = 100)
	professor = models.CharField(max_length = 100)
	className = models.CharField(max_length = 100, verbose_name="Class Name")
	date = models.CharField(max_length = 100)
	title = models.CharField(max_length = 100)
	paragraphs = models.TextField()
	citations = models.TextField()

	def __str__(self):
		return self.title[:50]

	def get_absolute_url(self):
		return reverse('chi-citation-details', args=[str(self.id)])